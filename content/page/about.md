---
layout: page
title: Sobre mí
permalink: /about/
bigimg: [{src: "/article_images/generic/yosoycola.png"}]
subtitle: 
comments: true
---
Mi nombre es Álex, conocido por aquí como Felfalido o simplemente **Felfa**.

En este blog me dedicaré de vez en cuando a publicar algunos artículos relacionados con mis principales hobbies o con otras trivialidades.

Promuevo y defiendo el uso de software libre en todos los ámbitos. Uso **GNU/Linux** desde el año 2008, tras haberme iniciado dos años antes con la distribución Debian [Sarge](/post/2016-10-26-mis-inicios-gnulinux).

Mi distribución actual de uso cotidiano es **Arch (GNU/)Linux** con escritorio KDE Plasma, tras haber hecho escala previamente en distribuciones derivadas de **Debian** y de **Slackware**, trabajando con distintos entornos de escritorio durante el tiempo (normalmente basados en GTK+).

![](https://i2.wp.com/karlaperezyt.com/wp-content/uploads/copito/viernes/11_2401293.png)

Empecé en el mundo de la informática a muy temprana edad con un IBM PS/1 modelo 2011 que era de mi padre. Contaba con una CPU Intel 80286, monitor con gráficos VGA, dos disqueteras (una de 3,5" y otra de 5,25"), un disco duro de 40 MB, dos módulos de RAM de 512 KiB, sin tarjeta de sonido ni módem.

Contaba con el sistema IBM DOS 4.0 incorporado en la propia ROM del equipo, con una interesante shell gráfica, Microsoft Works 2.0 y el intérprete BASIC A4.0 (más tarde se le instaló además el QuickBASIC 4.5).

Ese ordenador pasó a ¿mejor? vida durante el año 2006, cuando apenas al encender el monitor CRT, lo único que hacía era dar chispazos tras haberse dañado el circuito de deflexión vertical. De haberse podido reparar el fallo que tenía el monitor donde tenía integrada la ROM y la fuente de alimentación, se habría podido prolongar su vida útil hasta la actualidad.

![](/article_images/generic/ps1shell.png)