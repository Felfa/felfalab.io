 ---
layout: post
title:  "Rarezas jugables: PAC 2000"
date:   2021-10-10 17:53:41
categories: juegos
tags: ["pac2000", "juegos", "dosbox"]
bigimg: [{src: "/article_images/2021-10-12-rarezas-jugables-pac-2000/pac-hecho-polvo.png"}]
---
Después de 4 años, reaparezco por aquí para presentar (y poner a prueba por mí mismo) una función nueva que estoy incorporando a la web para poder incrustar instancias de DOSBox mediante Javascript.
<!--more-->
El motor utilizado para ello se llama [Emularity](https://github.com/db48x/emularity), el mismo que se usa en Web Archive.

## PAC 2000
__PAC 2000__ es un juego desarrollado por Mago Ediciones, SL. Esta empresa desarrollaba videojuegos de bajo coste para MS-DOS durante la última mitad de los años 90 y que solo llegaron a distribuirse en kioskos.

Este juego en particular es un clon del clásico [Pac-Man](https://es.wikipedia.org/wiki/Pac-Man), pero aquí manejas a un "comecocos" robótico que además puede obtener otros poderes adicionales, como ir más rápido o destruir a todos los enemigos de un plumazo. Tiene 5 fases distintas con dos niveles cada uno.

Los controles son simples: usa los botones de dirección para dirigir el comecocos a la dirección deseada. Puedes cambiar dichos controles desde el menú principal. Otros controles son: __[F1]__ para pausar el juego, __[F5]__ para de/silenciarlo y __[Esc]__ para... adivina para qué.

Si te apetece probar este juego, a continuación tendrás ocasión de hacerlo:

<html>
    <canvas id="canvas" style="width: 50%; height: 50%; display: block; margin: 0 auto;"/>
    <script type="text/javascript" src="/emulator_data/es6-promise.js"></script>
    <script type="text/javascript" src="/emulator_data/browserfs.min.js"></script>
    <script type="text/javascript" src="/emulator_data/loader.js"></script>
    <script type="text/javascript">
      var emulator = new Emulator(document.querySelector("#canvas"),
                                  null,
                                  new DosBoxLoader(DosBoxLoader.emulatorJS("/emulator_data/emulators/em-dosbox/dosbox-sync.js.gz"),
                                                   DosBoxLoader.locateAdditionalEmulatorJS(locateAdditionalFiles),
                                                   DosBoxLoader.nativeResolution(640, 480),
                                                   DosBoxLoader.mountZip("c",
                                                                         DosBoxLoader.fetchFile("Game File",
                                                                                                "https://cors.archive.org/cors/msdos_Pac_2000_1996/Pac_2000_1996.zip")),
                                                   DosBoxLoader.extraArgs(["-exit"]),
                                                   DosBoxLoader.startExe("pac2000\\pac.bat")))
      emulator.start({ waitAfterDownloading: false });
      function locateAdditionalFiles(filename) {
        if (filename === 'dosbox.html.mem') {
          return '/emulator_data/emulators/em-dosbox/dosbox-sync.mem.gz';
        }
        return '/emulator_data/emulators/em-dosbox/'+ filename;
      }
    </script>
</html>
Probado
