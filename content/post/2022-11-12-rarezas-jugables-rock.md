 ---
layout: post
title:  "Rarezas jugables: Rock"
date:   2022-11-12 17:14:26
categories: juegos
tags: [ "juegos", "dosbox"]
bigimg: [{src: "/article_images/2022-11-12-rarezas-jugables-rock/rock_u_000.png"}]
---
Vuelvo a activar este sitio con otra rareza jugable para DOS que he encontrado en un antiguo CD de una revista informática de los años 90. Este juego recordará a otro clásico de las máquinas de recreativas.
<!--more-->

## Rock
... y no tiene que ver con el género musical, sino más bien con la palabra inglesa "roca".

Este juego parece estar inspirado en el juego "Diamond Run" de Capcom, porque de hecho trata exactamente de lo mismo: sobrevivir dentro de las cuevas de cada nivel, recogiendo todos los objetos de valor que vayas encontrando sin que te caiga una roca encima. __Rock__ sin embargo tiene una estética diferente y no está desarrollada precisamente por una empresa.

Esta versión de __Rock__ que he incrustado en la web es shareware, por lo que notarás que tendrá ciertos límites, por ejemplo, solo tiene 5 niveles de los 20 que tenía la versión completa; pero igual es suficiente para pasar el rato.

Para obtener la versión completa, tenías que enviar unas 10 libras de la época al domicilio de Leslie Benzies o a Andrew McBean, que fueron los artífices de este videojuego. Una vez recibido el pago, ellos te devolvían por correo un disquete de 3,5 pulgadas con el juego completo y las instrucciones impresas adjuntas. Además, si te portabas bien y te gustaba el juego, te podían enviar algún que otro detalle como forma de agradecimiento también por vía postal. Vamos, una práctica muy común en aquella época para adquirir videojuegos a través de desarrolladoras independientes.

Los controles aquí son muy sencillos: usa los botones de dirección para manejar al personaje hacia la dirección indicada. Pulsa espacio + cualquier flecha de dirección para excavar en una casilla vecina. Puedes cambiar dichos controles desde el menú principal. Pulsa  __[Esc]__ en cualquier momento para salir al menú inicial.

__<span style="color:red">-!-</span>__ Es posible que el emulador no funcione correctamente en algunos navegadores.

<html>
    <canvas id="canvas" style="width: 50%; height: 50%; display: block; margin: 0 auto;"/>
    <script type="text/javascript" src="/emulator_data/es6-promise.js"></script>
    <script type="text/javascript" src="/emulator_data/browserfs.min.js"></script>
    <script type="text/javascript" src="/emulator_data/loader.js"></script>
    <script type="text/javascript">
      var emulator = new Emulator(document.querySelector("#canvas"),
                                  null,
                                  new DosBoxLoader(DosBoxLoader.emulatorJS("/emulator_data/emulators/em-dosbox/dosbox-sync.js.gz"),
                                                   DosBoxLoader.locateAdditionalEmulatorJS(locateAdditionalFiles),
                                                   DosBoxLoader.nativeResolution(640, 480),
                                                   DosBoxLoader.mountZip("c",
                                                                         DosBoxLoader.fetchFile("Game File",
                                                                                                "/emulator_data/resources/rock.zip")),
                                                   DosBoxLoader.extraArgs(["-exit"]),
                                                   DosBoxLoader.startExe("rock_u.exe")))
      emulator.start({ waitAfterDownloading: false });
      function locateAdditionalFiles(filename) {
        if (filename === 'dosbox.html.mem') {
          return '/emulator_data/emulators/em-dosbox/dosbox-sync.mem.gz';
        }
        return '/emulator_data/emulators/em-dosbox/'+ filename;
      }
    </script>
</html>